## Laravel Storage Provider

### License

This software is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)

#### Use

If you use the default storage config of `config/storage.php`, the service provider can be automatically loaded via
composer.

If you want to load your own, disable the automatic loading of `Smorken\Storage\ServiceProvider` in your project's
`composer.json` and create `App\Providers\StorageServiceProvider` that extends `Smorken\Storage\ServiceProvider`.

Add the service provider to `config/app.php`

```
    'providers' => [
    ...
    App\Providers\StorageServiceProvider::class,
    ...
```

##### Config

Create a `config/storage.php` with contents similar to the following:

```php
<?php
return [
    'concrete' => [
        \App\Storage\Eloquent\Menu::class => [
            'model' => [
                'impl'   => \App\Models\Eloquent\Menu::class,
                'params' => [],
            ],
        ],
    ],
    'contract' => [
        \App\Contracts\Storage\Menu::class => \App\Storage\Eloquent\Menu::class,
    ],
];
```

_App\Contracts\Storage\Menu_
```php
<?php
interface Menu extends \Smorken\Storage\Contracts\Base
{
    //contents
}
```

_App\Storage\Eloquent\Menu_
```php
<?php
class Menu extends \Smorken\Storage\Eloquent implements \App\Contracts\Storage\Menu
{
    //contents
}
```

_App\Contracts\Models\Menu_
```php
<?php
interface Menu extends \Smorken\Model\Contracts\Model
{
    //contents
}
```

_App\Models\Eloquent\Menu_
```php
<?php
class Menu extends \Smorken\Model\Eloquent implements \App\Contracts\Models\Menu
{
    //contents
}
```
