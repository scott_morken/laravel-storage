<?php

namespace Smorken\Storage;

use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Support\Collection;
use Smorken\Storage\Traits\CacheAssist;
use Smorken\Support\Contracts\Filter;

abstract class Base implements \Smorken\Storage\Contracts\CacheAssist, Contracts\Base
{
    use CacheAssist;

    protected mixed $model;

    protected array $validationRules = [];

    public function __construct($model, array $cacheOptions = [])
    {
        $this->setModel($model);
        if ($cacheOptions) {
            $this->cacheAssist = $this->createCacheAssist($cacheOptions);
        }
    }

    abstract protected function limitOrPaginate(Builder $query, int $perPage): Paginator|Collection|iterable;

    abstract protected function queryFromFilter(Filter $filter): Builder|Collection;

    public function getByFilter(Filter $filter, int $perPage = 20): Paginator|Collection|iterable
    {
        $query = $this->queryFromFilter($filter);

        return $this->limitOrPaginate($query, $perPage);
    }

    public function getModel(): mixed
    {
        return $this->model;
    }

    public function setModel($model): void
    {
        $this->model = $model;
    }

    public function validationRules(array $override = []): array
    {
        return [...$this->validationRules, ...$override];
    }

    protected function applyFilterToQuery(Filter $filter, Builder $query): Builder
    {
        if ($this->builderHasFilterMethod($query)) {
            $query = $query->filter($this->modifyFilterForBuilder($filter));
        }
        $filter_methods = $this->getFilterMethods();
        foreach ($filter->all() as $key => $value) {
            if (isset($filter_methods[$key]) && method_exists($this, $filter_methods[$key])) {
                $m = $filter_methods[$key];
                $query = $this->$m($query, $value);
            }
        }

        return $query;
    }

    protected function builderHasFilterMethod(Builder $builder): bool
    {
        return method_exists($builder, 'filter');
    }

    protected function modifyFilterForBuilder(Filter $filter): Filter
    {
        return $filter;
    }
}
