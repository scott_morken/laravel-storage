<?php

namespace Smorken\Storage\Constants;

class Cacheable
{
    public const ALL = 'all';

    public const ID = 'id';
}
