<?php

namespace Smorken\Storage\Contracts;

use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Support\Collection;
use Smorken\Support\Contracts\Filter;

interface Base
{
    public function all(): Collection|iterable;

    public function chunkFilter(Filter $filter, callable $chunk, int $count = 200): void;

    public function create(array $attributes): mixed;

    public function cursorFilter(Filter $filter): iterable;

    public function delete($model): bool;

    public function deleteById($id): bool;

    public function find($id): mixed;

    public function findOrFail($id): mixed;

    public function first(): mixed;

    public function firstOrCreate(array $identifiers, array $attributes = []): mixed;

    public function getByFilter(Filter $filter, int $perPage = 20): Paginator|Collection|iterable;

    public function getModel(): mixed;

    public function setModel($model): void;

    public function update($model, array $attributes): mixed;

    public function updateByIdentifier(array|string $identifier, array $attributes): bool;

    public function updateOrCreate(array $identifiers, array $attributes = []): mixed;

    public function validationRules(array $override = []): array;
}
