<?php

namespace Smorken\Storage\Contracts;

interface CacheAssist
{
    public function getCacheAssist(): \Smorken\CacheAssist\Contracts\CacheAssist;

    public function setCacheAssist(\Smorken\CacheAssist\Contracts\CacheAssist $cacheAssist): void;
}
