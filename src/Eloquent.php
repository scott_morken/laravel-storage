<?php

namespace Smorken\Storage;

use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Support\Collection;
use JetBrains\PhpStorm\ExpectedValues;
use Smorken\Storage\Constants\Cacheable;
use Smorken\Support\Contracts\Filter;

class Eloquent extends Base
{
    protected array $cacheable = [];

    protected array $forgetCacheable = [];

    public function all(): Collection|iterable
    {
        if ($this->wantsCaching(Cacheable::ALL)) {
            return $this->getCacheAssist()
                ->remember([Cacheable::ALL], $this->getCacheAssist()->getCacheOptions()->defaultCacheTime,
                    fn () => $this->getAllQuery()->get());
        }

        return $this->getAllQuery()->get();
    }

    public function chunkFilter(Filter $filter, callable $chunk, int $count = 200): void
    {
        $q = $this->queryFromFilter($filter);
        $q->chunk($count, $chunk);
    }

    public function create(array $attributes): mixed
    {
        $this->cacheForget();

        return $this->getModel()
            ->create($attributes);
    }

    public function cursorFilter(Filter $filter): iterable
    {
        $query = $this->queryFromFilter($filter);

        return $query->cursor();
    }

    public function delete($model): bool
    {
        if (is_object($model) && method_exists($model, 'delete')) {
            $this->cacheForgetByModel($model);

            return $model->delete();
        }

        return $this->deleteById($model);
    }

    public function deleteById($id): bool
    {
        if ($id) {
            $this->cacheForget($id, ! is_array($id));

            return $this->getModel()
                ->destroy($id);
        }

        return false;
    }

    public function find($id): mixed
    {
        if ($this->wantsCaching(Cacheable::ID)) {
            return $this->getCacheAssist()
                ->remember([$id], $this->getCacheAssist()->getCacheOptions()->defaultCacheTime,
                    fn () => $this->getFindQuery()->find($id));
        }

        return $this->getFindQuery()
            ->find($id);
    }

    public function findOrFail($id): mixed
    {
        if ($this->wantsCaching(Cacheable::ID)) {
            return $this->getCacheAssist()
                ->remember([$id], $this->getCacheAssist()->getCacheOptions()->defaultCacheTime,
                    fn () => $this->getFindQuery()->findOrFail($id));
        }

        return $this->getFindQuery()
            ->findOrFail($id);
    }

    public function first(): mixed
    {
        return $this->getModel()
            ->newQuery()
            ->defaultWiths()
            ->defaultOrder()
            ->first();
    }

    public function firstOrCreate(array $identifiers, array $attributes = []): mixed
    {
        $this->getCacheAssist()->forgetAuto();

        return $this->getModel()
            ->firstOrCreate($identifiers, $attributes);
    }

    public function update($model, array $attributes): mixed
    {
        $model->fill($attributes);
        $this->cacheForgetByModel($model);
        if ($model->save()) {
            return $model;
        }

        return null;
    }

    public function updateByIdentifier(array|string $identifier, array $attributes): bool
    {
        $query = $this->getModel()
            ->newQuery();
        if (! is_array($identifier)) {
            $pk = $this->getModel()
                ->getKeyName();
            $identifier[$pk] = $identifier;
        }
        foreach ($identifier as $k => $v) {
            $query->where($k, $v);
        }
        $this->cacheForget($identifier);

        return $query->update($attributes);
    }

    public function updateOrCreate(array $identifiers, array $attributes = []): mixed
    {
        $this->cacheForget($identifiers);

        return $this->getModel()
            ->updateOrCreate($identifiers, $attributes);
    }

    protected function cacheForget(mixed $id = null, bool $isSingle = true): void
    {
        if ($this->wantsCaching(Cacheable::ALL)) {
            $this->getCacheAssist()->forget([Cacheable::ALL]);
        }
        if ($this->wantsCaching(Cacheable::ID)) {
            $this->cacheForgetById($id, $isSingle);
        }
        foreach ($this->forgetCacheable as $key) {
            $this->getCacheAssist()->forget($key);
        }
        $this->getCacheAssist()->forgetAuto();
    }

    protected function cacheForgetById(mixed $id, bool $isSingle = true): void
    {
        if ($id !== null) {
            if ($isSingle) {
                $this->getCacheAssist()->forget($this->ensureCacheableIdentifier($id));
            } else {
                $id = (array) $id;
                foreach ($id as $i) {
                    $this->getCacheAssist()->forget($this->ensureCacheableIdentifier($i));
                }
            }
        }
    }

    protected function cacheForgetByModel($model): void
    {
        $id = $this->getIdFromModel($model);
        $this->cacheForget($id);
    }

    protected function ensureCacheableIdentifier(mixed $id): array
    {
        if (is_scalar($id)) {
            return [$id];
        }
        if (is_array($id)) {
            return [implode('', $id)];
        }

        return [(string) $this->getIdFromModel($id)];
    }

    protected function getAllQuery(): Builder
    {
        return $this->getBaseQuery();
    }

    protected function getBaseQuery(): Builder
    {
        return $this->getModel()
            ->newQuery()
            ->defaultWiths()
            ->defaultOrder();
    }

    protected function getFilterMethods(): array
    {
        return [];
    }

    protected function getFindQuery(): Builder
    {
        return $this->getModel()
            ->newQuery()
            ->defaultWiths();
    }

    protected function getIdFromModel($model): string|int|array|null
    {
        if (is_object($model) && method_exists($model, 'getKey')) {
            return $model->getKey();
        }
        try {
            return $model->id;
        } catch (\Throwable) {
            return null;
        }
    }

    protected function limitOrPaginate(Builder $query, int $perPage): Paginator|Collection|iterable
    {
        if ($perPage) {
            return $query->paginate($perPage);
        }

        return $query->limit(1000)
            ->get();
    }

    protected function queryFromFilter(Filter $filter): \Illuminate\Contracts\Database\Eloquent\Builder
    {
        $q = $this->getModel()
            ->newQuery()
            ->defaultWiths()
            ->defaultOrder();

        return $this->applyFilterToQuery($filter, $q);
    }

    protected function wantsCaching(#[ExpectedValues(valuesFromClass: Cacheable::class)] string $type): bool
    {
        return in_array($type, $this->cacheable);
    }
}
