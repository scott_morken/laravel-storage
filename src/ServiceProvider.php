<?php

namespace Smorken\Storage;

use Smorken\Support\Binder;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    public function register(): void
    {
        $binder = new Binder($this->app);
        $binder->bindAll($this->getStorageArray());
    }

    protected function getStorageArray()
    {
        return $this->app['config']->get('storage', []);
    }
}
