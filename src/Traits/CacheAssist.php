<?php

namespace Smorken\Storage\Traits;

use Smorken\CacheAssist\CacheOptions;

trait CacheAssist
{
    protected ?\Smorken\CacheAssist\Contracts\CacheAssist $cacheAssist = null;

    public function getCacheAssist(): \Smorken\CacheAssist\Contracts\CacheAssist
    {
        if (! $this->cacheAssist) {
            $this->cacheAssist = $this->createCacheAssist(['baseName' => $this::class]);
        }

        return $this->cacheAssist;
    }

    public function setCacheAssist(\Smorken\CacheAssist\Contracts\CacheAssist $cacheAssist): void
    {
        $this->cacheAssist = $cacheAssist;
    }

    protected function createCacheAssist(array $cacheOptions = []): \Smorken\CacheAssist\Contracts\CacheAssist
    {
        if (! isset($cacheOptions['baseName'])) {
            $cacheOptions['baseName'] = $this::class;
        }

        return new \Smorken\CacheAssist\CacheAssist(
            new CacheOptions(
                $cacheOptions
            )
        );
    }
}
