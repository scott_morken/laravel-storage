<?php

return [
    'concrete' => [
        \App\Storage\Eloquent\Menu::class => [
            'model' => [
                'impl' => \App\Models\Eloquent\Menu::class,
                'params' => [],
            ],
        ],
        \App\Storage\Rest\Menu::class => [
            'client' => [
                'impl' => \App\Clients\Guzzle::class,
                'params' => [
                    [
                        'url' => 'https://something.com',
                    ],
                ],
            ],
            'model' => [
                'impl' => \App\Models\VO\Menu::class,
            ],
        ],
        \App\Storage\Soap\Menu::class => [
            'client' => [
                'bound' => \App\Contracts\Soap::class,
            ],
            'model' => [
                'impl' => \App\Models\VO\Menu::class,
            ],
        ],
    ],
    'contract' => [
        \App\Contracts\Storage\Menu::class => \App\Storage\Eloquent\Menu::class,
    ],
];
