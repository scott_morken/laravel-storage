<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/11/16
 * Time: 3:51 PM
 */

namespace Tests\Smorken\Storage\Unit;

use Carbon\Carbon;
use Illuminate\Cache\CacheManager;
use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Support\Collection;
use Mockery as m;
use Smorken\CacheAssist\CacheAssist;
use Smorken\CacheAssist\CacheOptions;
use Smorken\Model\Contracts\Model;
use Smorken\Storage\Constants\Cacheable;
use Smorken\Storage\Eloquent;

class EloquentStorageTest extends \PHPUnit\Framework\TestCase
{
    public function testAll(): void
    {
        [$sut, $model] = $this->getSutAndModel();
        $query = m::mock(Builder::class);
        $model->shouldReceive('newQuery')
            ->once()
            ->andReturn($query);
        $query->shouldReceive('defaultWiths')
            ->andReturnSelf();
        $query->shouldReceive('defaultOrder')
            ->andReturnSelf();
        $results = new Collection();
        $query->shouldReceive('get')
            ->once()
            ->andReturn($results);
        $sut->getCacheAssist()->getCache()->shouldReceive('has')
            ->once()
            ->with('TestsSmorkenStorageUnitEloquentStub/all')
            ->andReturn(false);
        $sut->getCacheAssist()->getCache()->shouldReceive('put')
            ->once()
            ->with('TestsSmorkenStorageUnitEloquentStub/all', $results, m::type(Carbon::class))
            ->andReturn(true);
        $this->assertEquals($results, $sut->all());
    }

    public function testDeleteByIdWithArray(): void
    {
        [$sut, $model] = $this->getSutAndModel();
        $model->shouldReceive('destroy')
            ->once()
            ->with([1, 2])
            ->andReturn(true);
        $sut->getCacheAssist()->getCache()->shouldReceive('forget')->once()->with(
            'TestsSmorkenStorageUnitEloquentStub/1'
        )->andReturn(1);
        $sut->getCacheAssist()->getCache()->shouldReceive('forget')->once()->with(
            'TestsSmorkenStorageUnitEloquentStub/2'
        )->andReturn(1);
        $sut->getCacheAssist()->getCache()->shouldReceive('forget')->once()->with(
            'TestsSmorkenStorageUnitEloquentStub/all'
        )->andReturn(0);
        $this->assertEquals(2, $sut->deleteById([1, 2]));
    }

    public function testDeleteWithId(): void
    {
        [$sut, $model] = $this->getSutAndModel();
        $model->shouldReceive('destroy')
            ->once()
            ->with(1)
            ->andReturn(true);
        $sut->getCacheAssist()->getCache()->shouldReceive('forget')->once()->with(
            'TestsSmorkenStorageUnitEloquentStub/1'
        )->andReturn(1);
        $sut->getCacheAssist()->getCache()->shouldReceive('forget')->once()->with(
            'TestsSmorkenStorageUnitEloquentStub/all'
        )->andReturn(0);
        $this->assertTrue($sut->delete(1));
    }

    public function testDeleteWithModel(): void
    {
        [$sut, $model] = $this->getSutAndModel();
        $model->shouldReceive('destroy')
            ->andReturn(true);
        $model->shouldReceive('getKey')
            ->andReturn(123);
        $model->id = 123;
        $sut->getCacheAssist()->getCache()->shouldReceive('forget')->once()->with(
            'TestsSmorkenStorageUnitEloquentStub/123'
        )->andReturn(1);
        $sut->getCacheAssist()->getCache()->shouldReceive('forget')->once()->with(
            'TestsSmorkenStorageUnitEloquentStub/all'
        )->andReturn(0);
        $this->assertTrue($sut->delete($model));
    }

    public function testFilteredLimited(): void
    {
        [$sut, $model] = $this->getSutAndModel();
        $filter = new \Smorken\Support\Filter(['foo' => 'bar']);
        $query = m::mock(Builder::class);
        $model->shouldReceive('newQuery')
            ->once()
            ->andReturn($query);
        $query->shouldReceive('defaultWiths')
            ->andReturnSelf();
        $query->shouldReceive('defaultOrder')
            ->andReturnSelf();
        $query->shouldReceive('foo')
            ->with('bar')
            ->andReturn($query);
        $result = new Collection();
        $query->shouldReceive('limit->get')
            ->once()
            ->andReturn($result);
        $this->assertEquals($result, $sut->getByFilter($filter, 0));
    }

    public function testFilteredPaginated(): void
    {
        [$sut, $model] = $this->getSutAndModel();
        $filter = new \Smorken\Support\Filter(['foo' => 'bar']);
        $query = m::mock(Builder::class);
        $model->shouldReceive('newQuery')
            ->once()
            ->andReturn($query);
        $query->shouldReceive('foo')
            ->with('bar')
            ->andReturn($query);
        $query->shouldReceive('defaultWiths')
            ->andReturnSelf();
        $query->shouldReceive('defaultOrder')
            ->andReturnSelf();
        $result = new Collection();
        $query->shouldReceive('paginate')
            ->once()
            ->andReturn($result);
        $this->assertEquals($result, $sut->getByFilter($filter));
    }

    public function testFind(): void
    {
        [$sut, $model] = $this->getSutAndModel();
        $query = m::mock(Builder::class);
        $model->shouldReceive('newQuery')
            ->once()
            ->andReturn($query);
        $query->shouldReceive('defaultWiths')
            ->andReturnSelf();
        $query->shouldReceive('find')
            ->once()
            ->with(1)
            ->andReturn('model');
        $sut->getCacheAssist()->getCache()->shouldReceive('has')
            ->once()
            ->with('TestsSmorkenStorageUnitEloquentStub/1')
            ->andReturn(false);
        $sut->getCacheAssist()->getCache()->shouldReceive('put')
            ->once()
            ->with('TestsSmorkenStorageUnitEloquentStub/1', 'model', m::type(Carbon::class))
            ->andReturn(true);
        $this->assertEquals('model', $sut->find(1));
    }

    public function testFirst(): void
    {
        [$sut, $model] = $this->getSutAndModel();
        $query = m::mock('Query');
        $model->shouldReceive('newQuery')
            ->once()
            ->andReturn($query);
        $query->shouldReceive('defaultWiths')
            ->andReturnSelf();
        $query->shouldReceive('defaultOrder')
            ->andReturnSelf();
        $query->shouldReceive('first')
            ->once()
            ->andReturn('model');
        $this->assertEquals('model', $sut->first());
    }

    public function testUpdateByArrayIdentifier(): void
    {
        [$sut, $model] = $this->getSutAndModel();
        $query = m::mock(Builder::class);
        $model->shouldReceive('newQuery')
            ->once()
            ->andReturn($query);
        $query->shouldReceive('where')
            ->once()
            ->with('k1', 12)
            ->andReturn($query);
        $query->shouldReceive('where')
            ->once()
            ->with('k2', 'foo')
            ->andReturn($query);
        $query->shouldReceive('where')
            ->once()
            ->with('k3', 'bar')
            ->andReturn($query);
        $sut->getCacheAssist()->getCache()->shouldReceive('forget')->once()->with(
            'TestsSmorkenStorageUnitEloquentStub/12foobar'
        )->andReturn(1);
        $sut->getCacheAssist()->getCache()->shouldReceive('forget')->once()->with(
            'TestsSmorkenStorageUnitEloquentStub/all'
        )->andReturn(1);
        $query->shouldReceive('update')
            ->once()
            ->with(['key' => 'value'])
            ->andReturn(true);
        $this->assertTrue($sut->updateByIdentifier(['k1' => 12, 'k2' => 'foo', 'k3' => 'bar'], ['key' => 'value']));
    }

    public function testUpdateOrCreate(): void
    {
        [$sut, $model] = $this->getSutAndModel();
        $sut->getCacheAssist()->getCache()->shouldReceive('forget')->once()->with(
            'TestsSmorkenStorageUnitEloquentStub/12foobar'
        )->andReturn(1);
        $sut->getCacheAssist()->getCache()->shouldReceive('forget')->once()->with(
            'TestsSmorkenStorageUnitEloquentStub/all'
        )->andReturn(1);
        $model->shouldReceive('updateOrCreate')
            ->once()
            ->with(['k1' => 12, 'k2' => 'foo', 'k3' => 'bar'], ['key' => 'value'])
            ->andReturn($model);
        $this->assertEquals($model,
            $sut->updateOrCreate(['k1' => 12, 'k2' => 'foo', 'k3' => 'bar'], ['key' => 'value']));
    }

    public function testUpdateWithModel(): void
    {
        [$sut, $model] = $this->getSutAndModel();
        $model->shouldReceive('fill')
            ->once()
            ->with(['key' => 'value']);
        $model->id = 123;
        $model->shouldReceive('getKey')
            ->once()
            ->andReturn(123);
        $sut->getCacheAssist()->getCache()->shouldReceive('forget')->once()->with(
            'TestsSmorkenStorageUnitEloquentStub/123'
        )->andReturn(1);
        $sut->getCacheAssist()->getCache()->shouldReceive('forget')->once()->with(
            'TestsSmorkenStorageUnitEloquentStub/all'
        )->andReturn(1);
        $model->shouldReceive('save')
            ->once()
            ->andReturn(true);
        $this->assertEquals($model, $sut->update($model, ['key' => 'value']));
    }

    public function testWithOverriddenCacheOptionsAutoForget(): void
    {
        $repo = m::mock(CacheManager::class);
        CacheAssist::setCache($repo);
        $model = m::mock(\Smorken\Model\Eloquent::class);
        $sut = new EloquentStub($model, ['forgetAuto' => ['foo']]);
        $this->assertEquals(['foo'], $sut->getCacheAssist()->getCacheOptions()->forgetAuto);
        $this->assertEquals(
            'Tests\Smorken\Storage\Unit\EloquentStub',
            $sut->getCacheAssist()->getCacheOptions()->baseName
        );
    }

    public function testWithOverriddenCacheOptionsBaseName(): void
    {
        $repo = m::mock(CacheManager::class);
        CacheAssist::setCache($repo);
        $model = m::mock(\Smorken\Model\Eloquent::class);
        $sut = new EloquentStub($model, ['baseName' => 'foo']);
        $this->assertEquals([], $sut->getCacheAssist()->getCacheOptions()->forgetAuto);
        $this->assertEquals(
            'foo',
            $sut->getCacheAssist()->getCacheOptions()->baseName
        );
    }

    protected function getSutAndModel(): array
    {
        $repo = m::mock(CacheManager::class);
        CacheAssist::setCache($repo);
        $cache = new CacheAssist(new CacheOptions(['baseName' => EloquentStub::class]));
        $model = m::mock(Model::class);
        $sut = new EloquentStub($model);
        $sut->setCacheAssist($cache);

        return [$sut, $model];
    }

    protected function tearDown(): void
    {
        m::close();
    }
}

class EloquentStub extends Eloquent
{
    protected array $cacheable = [Cacheable::ALL, Cacheable::ID];
}
